USE [master]
GO
/****** Object:  Database [complain_database]    Script Date: 09/04/2017 8:35:38 PM ******/
CREATE DATABASE [complain_database]

GO
ALTER DATABASE [complain_database] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [complain_database] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [complain_database] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [complain_database] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [complain_database] SET ARITHABORT OFF 
GO
ALTER DATABASE [complain_database] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [complain_database] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [complain_database] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [complain_database] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [complain_database] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [complain_database] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [complain_database] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [complain_database] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [complain_database] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [complain_database] SET  ENABLE_BROKER 
GO
ALTER DATABASE [complain_database] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [complain_database] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [complain_database] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [complain_database] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [complain_database] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [complain_database] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [complain_database] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [complain_database] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [complain_database] SET  MULTI_USER 
GO
ALTER DATABASE [complain_database] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [complain_database] SET DB_CHAINING OFF 
GO
ALTER DATABASE [complain_database] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [complain_database] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [complain_database] SET DELAYED_DURABILITY = DISABLED 
GO
USE [complain_database]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 09/04/2017 8:35:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[Id_Account] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Gender] [bit] NULL,
	[Brithday] [smalldatetime] NULL,
	[Email] [varchar](50) NULL,
	[Avatar] [varchar](50) NULL,
	[Id_faculty] [int] NOT NULL,
	[Id_department] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Account] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Claims]    Script Date: 09/04/2017 8:35:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Claims](
	[Id_claims] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Description] [text] NULL,
	[Pdf] [nvarchar](200) NULL,
	[Status] [int] NULL,
	[Id_department] [int] NOT NULL,
	[Id_Account] [int] NOT NULL,
	[CreateTime] [smalldatetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_claims] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Department]    Script Date: 09/04/2017 8:35:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department](
	[Id_department] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_department] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Faculty]    Script Date: 09/04/2017 8:35:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faculty](
	[Id_faculty] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_faculty] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([Id_Account], [Username], [Password], [Name], [Gender], [Brithday], [Email], [Avatar], [Id_faculty], [Id_department]) VALUES (3, N'student1', N'123', N'Student 1', 1, CAST(N'1992-11-16 00:00:00' AS SmallDateTime), N'student1@gmail.com', NULL, 1, 1)
INSERT [dbo].[Account] ([Id_Account], [Username], [Password], [Name], [Gender], [Brithday], [Email], [Avatar], [Id_faculty], [Id_department]) VALUES (4, N'student2', N'123', N'Student 2', 1, CAST(N'1992-11-16 00:00:00' AS SmallDateTime), N'student2@gmail.com', NULL, 1, 1)
INSERT [dbo].[Account] ([Id_Account], [Username], [Password], [Name], [Gender], [Brithday], [Email], [Avatar], [Id_faculty], [Id_department]) VALUES (5, N'student3', N'123', N'Student 3', 1, CAST(N'1992-11-16 00:00:00' AS SmallDateTime), N'student3@gmail.com', NULL, 1, 1)
INSERT [dbo].[Account] ([Id_Account], [Username], [Password], [Name], [Gender], [Brithday], [Email], [Avatar], [Id_faculty], [Id_department]) VALUES (6, N'admin', N'123', N'admin', 0, CAST(N'1995-11-16 00:00:00' AS SmallDateTime), N'admin@gmail.com', NULL, 2, 1)
INSERT [dbo].[Account] ([Id_Account], [Username], [Password], [Name], [Gender], [Brithday], [Email], [Avatar], [Id_faculty], [Id_department]) VALUES (7, N'unmanager1', N'123', N'University Manager', 1, CAST(N'1988-11-16 00:00:00' AS SmallDateTime), N'unmanager@gmail.com', NULL, 4, 4)
INSERT [dbo].[Account] ([Id_Account], [Username], [Password], [Name], [Gender], [Brithday], [Email], [Avatar], [Id_faculty], [Id_department]) VALUES (8, N'fc1', N'123', N'Faculty Coordinator 1', 0, CAST(N'1989-11-16 00:00:00' AS SmallDateTime), N'coordinator@gmail.com', NULL, 3, 3)
INSERT [dbo].[Account] ([Id_Account], [Username], [Password], [Name], [Gender], [Brithday], [Email], [Avatar], [Id_faculty], [Id_department]) VALUES (10, N'fc2', N'123', N'Faculty Coordinator 1', 1, CAST(N'1990-11-16 00:00:00' AS SmallDateTime), N'coordinator1@Gmail.com', NULL, 3, 2)
SET IDENTITY_INSERT [dbo].[Account] OFF
SET IDENTITY_INSERT [dbo].[Claims] ON 

INSERT [dbo].[Claims] ([Id_claims], [Title], [Description], [Pdf], [Status], [Id_department], [Id_Account], [CreateTime]) VALUES (29, N'My score isn''t corect', N'My score isn''t corect', N'F:\Complain_PJ\Complain\Complain\file_pdf\CV ITC.pdf', 1, 3, 3, CAST(N'2017-04-09 12:29:00' AS SmallDateTime))
INSERT [dbo].[Claims] ([Id_claims], [Title], [Description], [Pdf], [Status], [Id_department], [Id_Account], [CreateTime]) VALUES (30, N'Warning', N'Warning', N'F:\Complain_PJ\Complain\Complain\file_pdf\CV ITR.pdf', 2, 2, 3, CAST(N'2017-04-09 16:57:00' AS SmallDateTime))
INSERT [dbo].[Claims] ([Id_claims], [Title], [Description], [Pdf], [Status], [Id_department], [Id_Account], [CreateTime]) VALUES (31, N'Warning', N'Warning', N'F:\Complain_PJ\Complain\Complain\file_pdf\CV ITI.pdf', 1, 3, 3, CAST(N'2017-04-09 17:34:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Claims] OFF
SET IDENTITY_INSERT [dbo].[Department] ON 

INSERT [dbo].[Department] ([Id_department], [Name]) VALUES (1, N'Admin')
INSERT [dbo].[Department] ([Id_department], [Name]) VALUES (2, N'Development economics')
INSERT [dbo].[Department] ([Id_department], [Name]) VALUES (3, N'International Payment')
INSERT [dbo].[Department] ([Id_department], [Name]) VALUES (4, N'Computer science')
SET IDENTITY_INSERT [dbo].[Department] OFF
SET IDENTITY_INSERT [dbo].[Faculty] ON 

INSERT [dbo].[Faculty] ([Id_faculty], [Name]) VALUES (1, N'Student')
INSERT [dbo].[Faculty] ([Id_faculty], [Name]) VALUES (2, N'Admin')
INSERT [dbo].[Faculty] ([Id_faculty], [Name]) VALUES (3, N'Coordinator')
INSERT [dbo].[Faculty] ([Id_faculty], [Name]) VALUES (4, N'Unmanager')
SET IDENTITY_INSERT [dbo].[Faculty] OFF
ALTER TABLE [dbo].[Account]  WITH CHECK ADD FOREIGN KEY([Id_department])
REFERENCES [dbo].[Department] ([Id_department])
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD FOREIGN KEY([Id_faculty])
REFERENCES [dbo].[Faculty] ([Id_faculty])
GO
ALTER TABLE [dbo].[Claims]  WITH CHECK ADD FOREIGN KEY([Id_Account])
REFERENCES [dbo].[Account] ([Id_Account])
GO
ALTER TABLE [dbo].[Claims]  WITH CHECK ADD FOREIGN KEY([Id_department])
REFERENCES [dbo].[Department] ([Id_department])
GO
USE [master]
GO
ALTER DATABASE [complain_database] SET  READ_WRITE 
GO

