﻿using Complain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Complain.Controllers
{
    public class LoginController : Controller
    {
        complain_databaseEntities db = new complain_databaseEntities();
        // GET: Login
        #region Login
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection a)
        {
                var username = a["username"];
                var password = a["password"];
                ViewBag.Thongbao = "Tên đăng nhập hoặc mật khẩu không đúng";
                Account account = db.Accounts.Where(u => u.Username == username && u.Password == password).SingleOrDefault();
                if (account != null)
                {
                    Session["account_id"] = account.Id_Account;
                    Session["name"] = account.Username;
                    Session["faculty"] = account.Id_faculty;
                    Session["department"] = account.Id_department;
                    FormsAuthentication.SetAuthCookie(account.Id_faculty.ToString(),true);
                if (account.Id_faculty == 1)
                    {
                        return RedirectToAction("Index", "Student");
                    }
                    else 
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                }
                else
                {
                return View();
                    //ViewBag.Thongbao = "Tên đăng nhập hoặc mật khẩu không đúng";
                }
        }
        #endregion
    }
}