﻿using Complain.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Complain.Controllers
{
    [Authorize]
    public class StudentController : Controller
    {
        complain_databaseEntities db = new complain_databaseEntities();
        // GET: Student
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Id_department = new SelectList(db.Departments.Where(d=>d.Id_department != 1 && d.Id_department != 4).ToList(), "Id_department", "Name");
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(Claim cl, HttpPostedFileBase upload, FormCollection a)
        {
            ViewBag.Id_department = new SelectList(db.Departments.Where(d => d.Id_department != 1 && d.Id_department != 4).ToList(), "Id_department", "Name");
            cl.Id_Account = int.Parse(Session["account_id"].ToString());
            cl.CreateTime = DateTime.Now;
            var filename = Path.GetFileName(upload.FileName);
            var path = Path.Combine(Server.MapPath("~/file_pdf"), filename);
            string[] kt = filename.Split(new char[] { '.' });
            string head = kt[0].ToString();
            cl.Pdf = head + ".pdf";
            while (System.IO.File.Exists(path))
            {                              
                Random rd = new Random();
                string TextRd;
                TextRd = Convert.ToString((char)rd.Next(65, 90));
                cl.Pdf = head + TextRd + ".pdf";
                path = Path.Combine(Server.MapPath("~/file_pdf"), head + TextRd + ".pdf");
            }
            upload.SaveAs(path);
            cl.Status = 0;
            db.Claims.Add(cl);
            db.SaveChanges();
            return Content("<script language='javascript' type='text/javascript'>alert('Thanks for Feedback!'); window.location.href ='/Student/Index'; </script>");
        }
    }
}