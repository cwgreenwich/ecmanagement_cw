﻿using Complain.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Complain.Controllers
{
    public class AdminController : Controller
    {
        complain_databaseEntities db = new complain_databaseEntities();
        #region Log out destroy session
        [Authorize]
        public ActionResult Exit()
        {
            Session["account_id"] = null;
            Session["name"] = null;
            Session["faculty"] = null;
            Session["department"] =null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Login","Login");
        }
        #endregion

        #region Home page with list claim
        [Authorize]
        public ActionResult Index(int? page)
        {
                int department = int.Parse(Session["department"].ToString());
                int pageNumber = (page ?? 1);
                int pageSize = 10;
                if (department != 1 && department != 4)
                {
                    return View(db.Claims.Where(c => c.Id_department == department).ToList().OrderBy(n => n.Id_department).ToPagedList(pageNumber, pageSize));
                }
                else
                {
                    return View(db.Claims.ToList().OrderBy(n => n.Id_department).ToPagedList(pageNumber, pageSize));
                }                   
        }
        #endregion

        #region Count number claim with every status
        [Authorize]
        public ActionResult CountClaim()
        {
            int department = int.Parse(Session["department"].ToString());
            List<int> count = new List<int>();
            if(department != 1 && department != 4)
            {
                int success = (from c in db.Claims
                               where c.Status == 2 && c.Id_department == department
                               select c).Count();
                count.Add(success);

                int inprogress = (from c in db.Claims
                                  where c.Status == 1 && c.Id_department == department
                                  select c).Count();
                count.Add(inprogress);
                int waiting = (from c in db.Claims
                               where c.Status == 0 && c.Id_department == department
                               select c).Count();
                count.Add(waiting);


            }
            else
            {
                int waiting = (from c in db.Claims
                               where c.Status == 0 
                               select c).Count();               
                int inprogress = (from c in db.Claims
                                  where c.Status == 1
                                  select c).Count();            

                int success = (from c in db.Claims
                               where c.Status == 2 
                               select c).Count();
                count.Add(success);
                count.Add(inprogress);
                count.Add(waiting);
            }
            

            return PartialView(count);
        }

        #endregion

        #region Detail Claim
        public ActionResult DetailClaim(int id)
        {
                //update status
                Claim cl = db.Claims.SingleOrDefault(n => n.Id_claims == id);
                if(cl.Status !=2)
                {
                    cl.Status = 1;
                }              
                UpdateModel(cl);
                db.SaveChanges();
                if (cl == null)
                {
                    Response.StatusCode = 404;
                    return null;
                }
                return View(cl);
        }
        [Authorize(Users = "2,3")]
        [HttpPost, ActionName("DetailClaim")]
        public ActionResult UpdateSuccess(int id)
        {        
                Claim cl = db.Claims.SingleOrDefault(n => n.Id_claims == id);
                cl.Status = 2;
                UpdateModel(cl);
                db.SaveChanges();
                return RedirectToAction("Index");
           
        }
        #endregion

        #region Edit Claim
        [Authorize(Users = "2,3")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
                ViewBag.Id_department = new SelectList(db.Departments.Where(d => d.Id_department != 1 && d.Id_department != 4).ToList(), "Id_department", "Name");
                Claim cl = db.Claims.SingleOrDefault(n => n.Id_claims == id);
                if (cl == null)
                {
                    Response.StatusCode = 404;
                    return null;
                }
                return View(cl);
        }
        [HttpPost]
        public ActionResult Edit(Claim cl, FormCollection a )
        {
            ViewBag.Id_department = new SelectList(db.Departments.Where(d => d.Id_department != 1 && d.Id_department != 4).ToList(), "Id_department", "Name");
            Claim cl_edit = db.Claims.SingleOrDefault(n => n.Id_claims == cl.Id_claims);
            UpdateModel(cl_edit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Delete Claim
        [Authorize(Users = "2,3")]
        public ActionResult Delete(int id)
        {
                Claim cl = db.Claims.SingleOrDefault(n => n.Id_claims == id);
                if (cl == null)
                {
                    Response.StatusCode = 404;
                    return null;
                }
                return View(cl);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult ConfirmDelete(int id)
        {
                Claim cl = db.Claims.SingleOrDefault(n => n.Id_claims == id);
                if (cl == null)
                {
                    Response.StatusCode = 404;
                    return null;
                }
                db.Claims.Remove(cl);
                db.SaveChanges();
                return RedirectToAction("Index");
        }
        #endregion

        #region List Claim With Status
        [Authorize]
        public ActionResult ListStatus(int? page,int status)
        {
                int department = int.Parse(Session["department"].ToString());
                int pageNumber = (page ?? 1);
                int pageSize = 10;
                if (department != 1 && department != 4)
                {
                    return View(db.Claims.Where(c => c.Id_department == department && c.Status == status).ToList().OrderBy(n => n.Id_department).ToPagedList(pageNumber, pageSize));
                }
                else
                {
                    return View(db.Claims.Where(c => c.Status == status).ToList().OrderBy(n => n.Id_department).ToPagedList(pageNumber, pageSize));
                }
        }
        #endregion

        #region User Info
        [Authorize]
        [HttpGet]
        public ActionResult UserInfo()
        {
            int user_id = int.Parse(Session["account_id"].ToString());
            Account ac = db.Accounts.Where(acc => acc.Id_Account == user_id).SingleOrDefault();
            return View(ac);
        }
        #endregion

        #region List account
        [Authorize(Users ="2")]
        [HttpGet]
        public ActionResult ListAccount()
        {        
            return View(db.Accounts.ToList());
        }
        #endregion

        #region Edit account
        [Authorize(Users = "2")]
        [HttpGet]
        public ActionResult EditAccount(int id)
        {
            ViewBag.Id_department = new SelectList(db.Departments.ToList(), "Id_department", "Name");
            ViewBag.Id_faculty = new SelectList(db.Faculties.ToList(), "Id_faculty", "Name");
            Account ac = db.Accounts.SingleOrDefault(n => n.Id_Account == id);
            if (ac == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(ac);
        }
        [HttpPost]
        public ActionResult EditAccount(Account ac, FormCollection a)
        {
            ViewBag.Id_department = new SelectList(db.Departments.ToList(), "Id_department", "Name");
            ViewBag.Id_faculty = new SelectList(db.Faculties.ToList(), "Id_faculty", "Name");
            Account ac_edit = db.Accounts.SingleOrDefault(n => n.Id_Account == ac.Id_Account);
            UpdateModel(ac_edit);
            db.SaveChanges();
            return RedirectToAction("ListAccount");
        }
        #endregion

        #region Create Account
        [Authorize(Users = "2")]
        [HttpGet]
        public ActionResult CreateAccount()
        {
            ViewBag.Id_department = new SelectList(db.Departments.ToList(), "Id_department", "Name");
            ViewBag.Id_faculty = new SelectList(db.Faculties.ToList(), "Id_faculty", "Name");
            return View();
        }
        [HttpPost]
        public ActionResult CreateAccount(Account ac)
        {
            db.Accounts.Add(ac);
            db.SaveChanges();
            return RedirectToAction("ListAccount");
        }
        #endregion

        #region detail account
        [Authorize(Users = "2")]
        public ActionResult DetailsAccount(int id)
        {           
            return View(db.Accounts.Where(ac=>ac.Id_Account==id).SingleOrDefault());
        }
        #endregion

        #region delete account
        [Authorize(Users = "2")]
        public ActionResult DeleteAccount(int id)
        {
            Account ac = db.Accounts.Where(acc => acc.Id_Account == id).SingleOrDefault();
            if (ac == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(ac);
        }
        [HttpPost, ActionName("DeleteAccount")]
        public ActionResult ConfirmDeleteAccount(int id)
        {
            Account ac = db.Accounts.Where(acc => acc.Id_Account == id).SingleOrDefault();
            if (ac == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.Accounts.Remove(ac);
            db.SaveChanges();
            return RedirectToAction("ListAccount");
        }
        #endregion

        #region dowload file pdf
        public ActionResult DownloadFile(String filename)
        {
            //string filepath = AppDomain.CurrentDomain.BaseDirectory + "/file_pdf/" + filename;
            string filepath = System.IO.Path.Combine(Server.MapPath("~/file_pdf"), filename);
            byte[] filedata = System.IO.File.ReadAllBytes(filepath);
            string contentType = MimeMapping.GetMimeMapping(filepath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = filename,
                Inline = true,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(filedata, contentType);
        }
        #endregion
    }

}